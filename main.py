from extrator_argumentos_url import ExtratorArgumentosUrl

"""
argumento = 'https://www.bytebank.com.br/cambio?moedaorigem=real'
substring = argumento[5:11]
print(substring)
"""
"""
argumento = 'https://www.bytebank.com.br/cambio?moedaorigem=real'
index = argumento.find('=')
substring = argumento[index + 1:]
print(substring)
"""

"""
argumento = 'moedaorigem=real'
lista_argumentos = argumento.split('=')
print(lista_argumentos)
"""


url = 'https://www.bytebank.com.br/cambio?moedaorigem=real&moedadestino=dolar'
argumento = ExtratorArgumentosUrl(url)

moeda_origem, moeda_destino = argumento.extrai_argumentos()
print(moeda_origem)
print(moeda_destino)

