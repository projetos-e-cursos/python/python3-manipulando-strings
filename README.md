# Python3 Manipulação de Strings

### Conceitos abordados:
- O que são Strings;
- Substrings;
- Fatiamento;
- find();
- split();
- len();
- replace();
- upper();
- lower();
- startswith();
- endswith();
- Expressões regulares;
- re.seach() e re.findall();
- Métodos especiais.

![string](/uploads/e7ed30d0658f3ba8d68636327d3a7687/string.gif)
