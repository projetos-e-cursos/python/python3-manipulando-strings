import re

email1 = 'Meu numero é 1234-1234'
email2 = 'Fale comigo em 1234-1234 esse é meu tel'
email3 = '1234-1234 é meu telefone'
email4 = 'allalalalalalalal 984528974 asdfasfawfawecaw acaweca 9784-9876 123455498  asdfa 9875-8484'

padrao = '[0-9]{4,5}[-]*[0-9]{4}'

# retorno = re.search(padrao, email3)
# print(retorno.group())

retorno = re.findall(padrao, email4)
for ret in retorno:
    print(ret)
